package com.example.duti.suchanaregistrationdata;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.duti.suchanaregistrationdata.api.ApiClient;
import com.example.duti.suchanaregistrationdata.api.ApiInterface;
import com.example.duti.suchanaregistrationdata.models.Ben;
import com.example.duti.suchanaregistrationdata.models.BenAllInfo;
import com.example.duti.suchanaregistrationdata.models.BenInformation;
import com.example.duti.suchanaregistrationdata.models.BenList;
import com.example.duti.suchanaregistrationdata.models.BenificiaryFewInfo;
import com.example.duti.suchanaregistrationdata.models.User;
import com.google.gson.GsonBuilder;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.duti.suchanaregistrationdata.utils.Constants.mApplicationType;

public class MainActivity extends AppCompatActivity {

    SweetAlertDialog uploadDialog;
    Button getDataBtn;
    int mInternalServerErrorCount = 0;
    private int c = 0;
    List<BenificiaryFewInfo> benificiaryFewInfoList = new ArrayList<BenificiaryFewInfo>();
    List<BenAllInfo> benAllInfoList = new ArrayList<BenAllInfo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init progress bar
        uploadDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);

        getDataBtn = (Button) findViewById(R.id.getData);
        getDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress();
                getBenList();
            }
        });
    }

    // create dialog for progress
    public void showProgress() {
        uploadDialog
                .setTitleText("Fetching Ben Info")
                .show();
        uploadDialog.setCancelable(false);
        uploadDialog.setCanceledOnTouchOutside(false);
        uploadDialog.getProgressHelper().setBarColor(Color.parseColor("#f44336"));
    }

    // hide progress dialog
    public void hideProgress() {
        // if previously showing it then hide that
        if (uploadDialog.isShowing()) {
            uploadDialog.dismiss();
        }
    }

    public void getBenList() {
        // call api interface where is API call
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        // create new request
        User user = new User("1058");

        // call Api interface to send data to the server
        Call<BenList> call = apiInterface.requestToGetBenList(mApplicationType, user);

        // get response from the server
        call.enqueue(new Callback<BenList>() {
            @Override
            public void onResponse(Call<BenList> call, Response<BenList> response) {
                BenList benList = response.body();
                Log.w("duti", " JSON: "+new GsonBuilder().setPrettyPrinting().create().toJson(benList));
                if (response.isSuccessful()) {
                    Log.d("duti", "*****Successful*****");
                    benificiaryFewInfoList = benList.getBenificiaryFewInfo();
                    Log.d("duti", "Ben List Size: "+String.valueOf(benificiaryFewInfoList.size()));
                    makeToast("You Have "+String.valueOf(benificiaryFewInfoList.size()+1)+ " Beneficiaries");
                    for(int i = 0; i<301; i++){
                        Log.d("duti", "Ben Id: "+benificiaryFewInfoList.get(i).getBenID());
                        getIndividualBenInfo(benificiaryFewInfoList.get(i).getBenID());
                    }
                    // end upload
                    mInternalServerErrorCount = 0;
                } else if (response.code() == 401) {
                    // Handle unauthorized
                    Log.d("duti", "text: " + response.toString());
                    makeToast(response.toString());
                    hideProgress();
                } else if (response.code() == 400) {
                    // Handle bad request
                    Log.d("duti", "text: " + response.toString());
                    makeToast(response.toString());
                    hideProgress();
                } else if (response.code() == 500) {
                    // Handle Internal Server Error
                    Log.d("duti", "text: " + response.toString());
                    // add counter to count error times
                    mInternalServerErrorCount++;
                    if (mInternalServerErrorCount < 4) {
                        // send to server once again
                        getBenList();
                    } else {
                        Log.d("duti", "text: " + response.toString());
                        makeToast(response.toString());
                        hideProgress();;
                    }

                } else {
                    // Handle other responses
                    Log.d("duti", "text: " + response.toString());
                    makeToast(response.toString());
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<BenList> call, Throwable t) {
                Log.d("duti", "text: " + t.getMessage());
                makeToast(t.getMessage());
                hideProgress();
            }
        });
    }

    public void getIndividualBenInfo(final String benId) {
        // call api interface where is API call
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        // create new request
        Ben ben = new Ben(benId);

        // call Api interface to send data to the server
        Call<BenInformation> call = apiInterface.requestToGetBenInfo(mApplicationType, ben);

        // get response from the server
        call.enqueue(new Callback<BenInformation>() {
            @Override
            public void onResponse(Call<BenInformation> call, Response<BenInformation> response) {
                BenInformation benInformation = response.body();
                Log.w("hello", " JSON: "+new GsonBuilder().setPrettyPrinting().create().toJson(benInformation));
                if (response.isSuccessful()) {
                    c++;
                    Log.d("hello", "*****Successful*****");
                    benAllInfoList = benInformation.getBenAllInfo();
                    Log.d("hello", "Ben Info List Size: "+String.valueOf(benAllInfoList.size()));
                    for(int i =0; i<benAllInfoList.size(); i++){
                        Log.d("hello", "Ben Details Id: "+benAllInfoList.get(i).getBenID());
                    }
                    // end upload
                    mInternalServerErrorCount = 0;
                    if (c == 301) {
                        hideProgress();
                        makeToast("Successfully fetched "+ String.valueOf(c-2)+" Ben Info");
                        c = 0;
                    }
                } else if (response.code() == 401) {
                    // Handle unauthorized
                    Log.d("hello", "text: " + response.toString());
                    makeToast(response.toString());
                    hideProgress();
                } else if (response.code() == 400) {
                    // Handle bad request
                    Log.d("hello", "text: " + response.toString());
                    makeToast(response.toString());
                    hideProgress();
                } else if (response.code() == 500) {
                    // Handle Internal Server Error
                    Log.d("hello", "text: " + response.toString());
                    // add counter to count error times
                    mInternalServerErrorCount++;
                    if (mInternalServerErrorCount < 4) {
                        // send to server once again
                        getIndividualBenInfo(benId);
                    } else {
                        Log.d("hello", "text: " + response.toString());
                        makeToast(response.toString());
                        hideProgress();;
                    }

                } else {
                    // Handle other responses
                    Log.d("hello", "text: " + response.toString());
                    makeToast(response.toString());
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<BenInformation> call, Throwable t) {
                Log.d("hello", "text: " + t.getMessage());
                makeToast(t.getMessage());
                hideProgress();
            }
        });
    }

    // method to make toast
    public void makeToast(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
