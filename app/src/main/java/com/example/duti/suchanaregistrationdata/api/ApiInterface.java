package com.example.duti.suchanaregistrationdata.api;


import com.example.duti.suchanaregistrationdata.models.Ben;
import com.example.duti.suchanaregistrationdata.models.BenInformation;
import com.example.duti.suchanaregistrationdata.models.BenList;
import com.example.duti.suchanaregistrationdata.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

import static com.example.duti.suchanaregistrationdata.utils.Constants.mContentType;

public interface ApiInterface {

    @POST("GetPhaseTwoRegistrationDataById")
    Call<BenList> requestToGetBenList(
            @Header(mContentType) String contentType,
            @Body User user);

    @POST("GetBenAllInfoByBenId")
    Call<BenInformation> requestToGetBenInfo(
            @Header(mContentType) String contentType,
            @Body Ben ben);

}
