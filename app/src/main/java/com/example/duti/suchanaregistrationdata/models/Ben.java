package com.example.duti.suchanaregistrationdata.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ben {
    public String getBenId() {
        return benId;
    }

    public void setBenId(String benId) {
        this.benId = benId;
    }

    @SerializedName("BenID")
    @Expose
    private String benId;

    public Ben(String benId){
        this.benId = benId;
    }
}
