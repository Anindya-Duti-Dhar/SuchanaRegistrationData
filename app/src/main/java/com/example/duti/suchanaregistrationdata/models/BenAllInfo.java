package com.example.duti.suchanaregistrationdata.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BenAllInfo {

    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("Phase")
    @Expose
    private String phase;
    @SerializedName("BenID")
    @Expose
    private String benID;
    @SerializedName("BenNameEn")
    @Expose
    private String benNameEn;
    @SerializedName("BenNameBn")
    @Expose
    private String benNameBn;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("NID")
    @Expose
    private String nID;
    @SerializedName("IpCode")
    @Expose
    private String ipCode;
    @SerializedName("DivisionCode")
    @Expose
    private String divisionCode;
    @SerializedName("DistrictCode")
    @Expose
    private String districtCode;
    @SerializedName("UpazilaCode")
    @Expose
    private String upazilaCode;
    @SerializedName("UnionCode")
    @Expose
    private String unionCode;
    @SerializedName("VillageCode")
    @Expose
    private String villageCode;
    @SerializedName("KhanaCode")
    @Expose
    private String khanaCode;
    @SerializedName("MaritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("EduLevel")
    @Expose
    private String eduLevel;
    @SerializedName("Occupation")
    @Expose
    private String occupation;
    @SerializedName("HasDisabled")
    @Expose
    private String hasDisabled;
    @SerializedName("DisablityType")
    @Expose
    private String disablityType;
    @SerializedName("DisMale")
    @Expose
    private String disMale;
    @SerializedName("DisFemale")
    @Expose
    private String disFemale;
    @SerializedName("HHHeadName")
    @Expose
    private String hHHeadName;
    @SerializedName("HHHeadSex")
    @Expose
    private String hHHeadSex;
    @SerializedName("HasPW")
    @Expose
    private String hasPW;
    @SerializedName("PWMonths")
    @Expose
    private String pWMonths;
    @SerializedName("CU2M")
    @Expose
    private String cU2M;
    @SerializedName("CU2F")
    @Expose
    private String cU2F;
    @SerializedName("CU2MDOB1")
    @Expose
    private String cU2MDOB1;
    @SerializedName("CU2MDOB1Text")
    @Expose
    private String cU2MDOB1Text;
    @SerializedName("CU2MDOB2")
    @Expose
    private String cU2MDOB2;
    @SerializedName("CU2MDOB2Text")
    @Expose
    private String cU2MDOB2Text;
    @SerializedName("CU2MDOB3")
    @Expose
    private String cU2MDOB3;
    @SerializedName("CU2MDOB3Text")
    @Expose
    private String cU2MDOB3Text;
    @SerializedName("CU2MDOB4")
    @Expose
    private String cU2MDOB4;
    @SerializedName("CU2MDOB4Text")
    @Expose
    private String cU2MDOB4Text;
    @SerializedName("CU2FDOB1")
    @Expose
    private String cU2FDOB1;
    @SerializedName("CU2FDOB1Text")
    @Expose
    private String cU2FDOB1Text;
    @SerializedName("CU2FDOB2")
    @Expose
    private String cU2FDOB2;
    @SerializedName("CU2FDOB2Text")
    @Expose
    private String cU2FDOB2Text;
    @SerializedName("CU2FDOB3")
    @Expose
    private String cU2FDOB3;
    @SerializedName("CU2FDOB3Text")
    @Expose
    private String cU2FDOB3Text;
    @SerializedName("CU2FDOB4")
    @Expose
    private String cU2FDOB4;
    @SerializedName("CU2FDOB4Text")
    @Expose
    private String cU2FDOB4Text;
    @SerializedName("CU2DOB1")
    @Expose
    private String cU2DOB1;
    @SerializedName("CU2DOB2")
    @Expose
    private String cU2DOB2;
    @SerializedName("CU2DOB3")
    @Expose
    private String cU2DOB3;
    @SerializedName("CU2DOB4")
    @Expose
    private String cU2DOB4;
    @SerializedName("CU2DOBSourceM")
    @Expose
    private String cU2DOBSourceM;
    @SerializedName("CU2DOBSourceF")
    @Expose
    private String cU2DOBSourceF;
    @SerializedName("CU2DOBSource")
    @Expose
    private String cU2DOBSource;
    @SerializedName("C2T5M")
    @Expose
    private String c2T5M;
    @SerializedName("C2T5F")
    @Expose
    private String c2T5F;
    @SerializedName("C6T14M")
    @Expose
    private String c6T14M;
    @SerializedName("C6T14F")
    @Expose
    private String c6T14F;
    @SerializedName("C15T19M")
    @Expose
    private String c15T19M;
    @SerializedName("C15T19F")
    @Expose
    private String c15T19F;
    @SerializedName("SGC6T19M")
    @Expose
    private String sGC6T19M;
    @SerializedName("SGC6T19F")
    @Expose
    private String sGC6T19F;
    @SerializedName("LabC6T14M")
    @Expose
    private String labC6T14M;
    @SerializedName("LabC6T14F")
    @Expose
    private String labC6T14F;
    @SerializedName("LabC6T14Type")
    @Expose
    private String labC6T14Type;
    @SerializedName("KhanaCodeWr")
    @Expose
    private String khanaCodeWr;
    @SerializedName("WRankNo")
    @Expose
    private String wRankNo;
    @SerializedName("WardNo")
    @Expose
    private String wardNo;
    @SerializedName("FatName")
    @Expose
    private String fatName;
    @SerializedName("FatType")
    @Expose
    private String fatType;
    @SerializedName("MotName")
    @Expose
    private String motName;
    @SerializedName("MotType")
    @Expose
    private String motType;
    @SerializedName("HusName")
    @Expose
    private String husName;
    @SerializedName("AsignOrg")
    @Expose
    private String asignOrg;
    @SerializedName("VerificationDate")
    @Expose
    private String verificationDate;
    @SerializedName("VerificationDateText")
    @Expose
    private String verificationDateText;
    @SerializedName("AsignOrgOthers")
    @Expose
    private String asignOrgOthers;
    @SerializedName("RegDate")
    @Expose
    private String regDate;
    @SerializedName("DataColYear")
    @Expose
    private String dataColYear;
    @SerializedName("DataEnum")
    @Expose
    private String dataEnum;
    @SerializedName("Cell")
    @Expose
    private String cell;
    @SerializedName("CellNeig")
    @Expose
    private String cellNeig;
    @SerializedName("HHMemberMale")
    @Expose
    private String hHMemberMale;
    @SerializedName("HHMemberFemale")
    @Expose
    private String hHMemberFemale;
    @SerializedName("HHMemberBoth")
    @Expose
    private String hHMemberBoth;
    @SerializedName("TempID")
    @Expose
    private String tempID;
    @SerializedName("InfoProvider")
    @Expose
    private String infoProvider;
    @SerializedName("InfoProviderRel")
    @Expose
    private String infoProviderRel;
    @SerializedName("BenDOB")
    @Expose
    private String benDOB;
    @SerializedName("IsRecommendedForDeletion")
    @Expose
    private String isRecommendedForDeletion;
    @SerializedName("ReasonForDeletion")
    @Expose
    private String reasonForDeletion;
    @SerializedName("ReasonDeleteOther")
    @Expose
    private String reasonDeleteOther;
    @SerializedName("ReasonForDropout")
    @Expose
    private String reasonForDropout;
    @SerializedName("ReasonDropOutOther")
    @Expose
    private String reasonDropOutOther;
    @SerializedName("SpecialComments")
    @Expose
    private String specialComments;
    @SerializedName("VerifiedStatus")
    @Expose
    private String verifiedStatus;
    @SerializedName("VerifiedDate")
    @Expose
    private String verifiedDate;
    @SerializedName("VerifiedBy")
    @Expose
    private String verifiedBy;
    @SerializedName("InsertDate")
    @Expose
    private String insertDate;
    @SerializedName("IsDropout")
    @Expose
    private String isDropout;
    @SerializedName("IsReasonForDropout")
    @Expose
    private String isReasonForDropout;
    @SerializedName("HFPSubgroup")
    @Expose
    private String hFPSubgroup;
    @SerializedName("IGAAssigned")
    @Expose
    private String iGAAssigned;
    @SerializedName("SG_A")
    @Expose
    private String sGA;
    @SerializedName("SG_M")
    @Expose
    private String sGM;
    @SerializedName("SG_SA")
    @Expose
    private String sGSA;
    @SerializedName("SG_SW")
    @Expose
    private String sGSW;
    @SerializedName("DV")
    @Expose
    private String dV;
    @SerializedName("VerifiedBenNameEn")
    @Expose
    private String verifiedBenNameEn;
    @SerializedName("NameChangedReason")
    @Expose
    private String nameChangedReason;
    @SerializedName("EarnerMale")
    @Expose
    private String earnerMale;
    @SerializedName("EarnerFemale")
    @Expose
    private String earnerFemale;
    @SerializedName("PrimeEarner")
    @Expose
    private String primeEarner;
    @SerializedName("PrimeEarnerTxt")
    @Expose
    private String primeEarnerTxt;
    @SerializedName("MoreEarnerNo")
    @Expose
    private String moreEarnerNo;
    @SerializedName("MoreEarnerName")
    @Expose
    private String moreEarnerName;
    @SerializedName("HasMoreEarner")
    @Expose
    private String hasMoreEarner;
    @SerializedName("PIncomeMonth")
    @Expose
    private String pIncomeMonth;
    @SerializedName("PIncomeDomain")
    @Expose
    private String pIncomeDomain;
    @SerializedName("PIncomeOtr")
    @Expose
    private String pIncomeOtr;
    @SerializedName("PIncomeOtrTxt")
    @Expose
    private String pIncomeOtrTxt;
    @SerializedName("MIncomeHas")
    @Expose
    private String mIncomeHas;
    @SerializedName("MIncomeDomain")
    @Expose
    private String mIncomeDomain;
    @SerializedName("MIncomeOtr")
    @Expose
    private String mIncomeOtr;
    @SerializedName("PIncomeTaka")
    @Expose
    private String pIncomeTaka;
    @SerializedName("MIncomeTaka")
    @Expose
    private String mIncomeTaka;
    @SerializedName("HasSsp")
    @Expose
    private String hasSsp;
    @SerializedName("SspCode")
    @Expose
    private String sspCode;
    @SerializedName("SspOthers")
    @Expose
    private String sspOthers;
    @SerializedName("HasBankAc")
    @Expose
    private String hasBankAc;
    @SerializedName("HasLoan")
    @Expose
    private String hasLoan;
    @SerializedName("LoanSrc1")
    @Expose
    private String loanSrc1;
    @SerializedName("LoanSrc2")
    @Expose
    private String loanSrc2;
    @SerializedName("LoanSrc3")
    @Expose
    private String loanSrc3;
    @SerializedName("LoanUse1")
    @Expose
    private String loanUse1;
    @SerializedName("LoanUse2")
    @Expose
    private String loanUse2;
    @SerializedName("LoanUse3")
    @Expose
    private String loanUse3;
    @SerializedName("LoanUseOtr")
    @Expose
    private String loanUseOtr;
    @SerializedName("LoanIntRate")
    @Expose
    private String loanIntRate;
    @SerializedName("LoanIntTaka")
    @Expose
    private String loanIntTaka;
    @SerializedName("LoanRepay1")
    @Expose
    private String loanRepay1;
    @SerializedName("LoanRepay2")
    @Expose
    private String loanRepay2;
    @SerializedName("LoanRepay3")
    @Expose
    private String loanRepay3;
    @SerializedName("LoanRepayOtr")
    @Expose
    private String loanRepayOtr;
    @SerializedName("HasSavings")
    @Expose
    private String hasSavings;
    @SerializedName("SaveingsOrg1")
    @Expose
    private String saveingsOrg1;
    @SerializedName("SaveingsOrg2")
    @Expose
    private String saveingsOrg2;
    @SerializedName("SaveingsOrg3")
    @Expose
    private String saveingsOrg3;
    @SerializedName("SaveingsOrgOtrs")
    @Expose
    private String saveingsOrgOtrs;
    @SerializedName("HsLandYn")
    @Expose
    private String hsLandYn;
    @SerializedName("HsLandDec")
    @Expose
    private String hsLandDec;
    @SerializedName("CuLandYn")
    @Expose
    private String cuLandYn;
    @SerializedName("CuLandDec")
    @Expose
    private String cuLandDec;
    @SerializedName("VegFrtYn")
    @Expose
    private String vegFrtYn;
    @SerializedName("VegFrtType")
    @Expose
    private String vegFrtType;
    @SerializedName("DukCknYn")
    @Expose
    private String dukCknYn;
    @SerializedName("CattleYn")
    @Expose
    private String cattleYn;
    @SerializedName("GoatSheepYn")
    @Expose
    private String goatSheepYn;
    @SerializedName("HfpUtil")
    @Expose
    private String hfpUtil;
    @SerializedName("PondYn")
    @Expose
    private String pondYn;
    @SerializedName("PondDec")
    @Expose
    private String pondDec;
    @SerializedName("PondType")
    @Expose
    private String pondType;
    @SerializedName("PondWaterMonth")
    @Expose
    private String pondWaterMonth;
    @SerializedName("PondOwnership")
    @Expose
    private String pondOwnership;
    @SerializedName("PondShareHolders")
    @Expose
    private String pondShareHolders;
    @SerializedName("AccessLakeYn")
    @Expose
    private String accessLakeYn;
    @SerializedName("LakeType")
    @Expose
    private String lakeType;
    @SerializedName("LakeShareHolders")
    @Expose
    private String lakeShareHolders;
    @SerializedName("LakeTypeOtr")
    @Expose
    private String lakeTypeOtr;
    @SerializedName("FCulYn")
    @Expose
    private String fCulYn;
    @SerializedName("FCulType")
    @Expose
    private String fCulType;
    @SerializedName("FCulTypeOtr")
    @Expose
    private String fCulTypeOtr;
    @SerializedName("FCulNotType1")
    @Expose
    private String fCulNotType1;
    @SerializedName("FCulNotType2")
    @Expose
    private String fCulNotType2;
    @SerializedName("FCulNotTypeOtr")
    @Expose
    private String fCulNotTypeOtr;
    @SerializedName("FBusYn")
    @Expose
    private String fBusYn;
    @SerializedName("FBusType")
    @Expose
    private String fBusType;
    @SerializedName("FBusOtr")
    @Expose
    private String fBusOtr;
    @SerializedName("FProdDryYn")
    @Expose
    private String fProdDryYn;
    @SerializedName("FProdDryUse")
    @Expose
    private String fProdDryUse;
    @SerializedName("BusExpBenYn")
    @Expose
    private String busExpBenYn;
    @SerializedName("BusExpMemYn")
    @Expose
    private String busExpMemYn;
    @SerializedName("TrainingYn")
    @Expose
    private String trainingYn;
    @SerializedName("TrainingType")
    @Expose
    private String trainingType;
    @SerializedName("TrainingTypeOtr")
    @Expose
    private String trainingTypeOtr;
    @SerializedName("DoingBusYn")
    @Expose
    private String doingBusYn;
    @SerializedName("DoingBusType")
    @Expose
    private String doingBusType;
    @SerializedName("Poverty")
    @Expose
    private String poverty;
    @SerializedName("VPAssetCow")
    @Expose
    private String vPAssetCow;
    @SerializedName("VPAssetPoultry")
    @Expose
    private String vPAssetPoultry;
    @SerializedName("VPAssetRickshaw")
    @Expose
    private String vPAssetRickshaw;
    @SerializedName("VPAssetSewingMac")
    @Expose
    private String vPAssetSewingMac;
    @SerializedName("VPAssetBicycle")
    @Expose
    private String vPAssetBicycle;
    @SerializedName("VPAssetPettyBus")
    @Expose
    private String vPAssetPettyBus;
    @SerializedName("VPAssetNursery")
    @Expose
    private String vPAssetNursery;
    @SerializedName("VPAssetBusiness")
    @Expose
    private String vPAssetBusiness;
    @SerializedName("VPAssetGoat")
    @Expose
    private String vPAssetGoat;
    @SerializedName("VPAssetBoat")
    @Expose
    private String vPAssetBoat;
    @SerializedName("VPAssetFfishnet")
    @Expose
    private String vPAssetFfishnet;
    @SerializedName("VPAssetSavings")
    @Expose
    private String vPAssetSavings;
    @SerializedName("VPAssetMobile")
    @Expose
    private String vPAssetMobile;
    @SerializedName("VPAssetMCycle")
    @Expose
    private String vPAssetMCycle;
    @SerializedName("VPAssetOtr1")
    @Expose
    private String vPAssetOtr1;
    @SerializedName("VPAssetOtr1num")
    @Expose
    private String vPAssetOtr1num;
    @SerializedName("VPAssetOtr2")
    @Expose
    private String vPAssetOtr2;
    @SerializedName("VPAssetOtr2num")
    @Expose
    private String vPAssetOtr2num;
    @SerializedName("VPAssetOtr3")
    @Expose
    private String vPAssetOtr3;
    @SerializedName("VPAssetOtr3num")
    @Expose
    private String vPAssetOtr3num;
    @SerializedName("ProductiveAssetsId")
    @Expose
    private String productiveAssetsId;
    @SerializedName("MaleAttendance")
    @Expose
    private String maleAttendance;
    @SerializedName("FemaleAttendance")
    @Expose
    private String femaleAttendance;
    @SerializedName("TotalAttendance")
    @Expose
    private String totalAttendance;
    @SerializedName("KhanaMemberComments")
    @Expose
    private String khanaMemberComments;
    @SerializedName("HhType")
    @Expose
    private String hhType;
    @SerializedName("BenType")
    @Expose
    private String benType;
    @SerializedName("PregnantOrLactating")
    @Expose
    private String pregnantOrLactating;
    @SerializedName("LandOwnershipType")
    @Expose
    private String landOwnershipType;
    @SerializedName("Roof")
    @Expose
    private String roof;
    @SerializedName("AroundOfHouse")
    @Expose
    private String aroundOfHouse;
    @SerializedName("Floor")
    @Expose
    private String floor;
    @SerializedName("LiveMode")
    @Expose
    private String liveMode;
    @SerializedName("LiveYears")
    @Expose
    private String liveYears;
    @SerializedName("HasMobileBankingAc")
    @Expose
    private String hasMobileBankingAc;
    @SerializedName("SaveingsPerson")
    @Expose
    private String saveingsPerson;
    @SerializedName("VPAssetBuffalo")
    @Expose
    private String vPAssetBuffalo;
    @SerializedName("VPAssetSheep")
    @Expose
    private String vPAssetSheep;
    @SerializedName("VPAssetDuck")
    @Expose
    private String vPAssetDuck;
    @SerializedName("VPAssetLivestockOthers")
    @Expose
    private String vPAssetLivestockOthers;
    @SerializedName("VPAssetTv")
    @Expose
    private String vPAssetTv;
    @SerializedName("VPAssetOtherFoodItems")
    @Expose
    private String vPAssetOtherFoodItems;
    @SerializedName("VPAssetOrnaments")
    @Expose
    private String vPAssetOrnaments;
    @SerializedName("VPAssetLargeTrees")
    @Expose
    private String vPAssetLargeTrees;
    @SerializedName("VPAssetTubewell")
    @Expose
    private String vPAssetTubewell;
    @SerializedName("VPAssetOthers")
    @Expose
    private String vPAssetOthers;
    @SerializedName("DukCknUtil")
    @Expose
    private String dukCknUtil;
    @SerializedName("FloodAffectdPondMonth")
    @Expose
    private String floodAffectdPondMonth;
    @SerializedName("GovernmentAgency")
    @Expose
    private String governmentAgency;
    @SerializedName("PrivateAgency")
    @Expose
    private String privateAgency;
    @SerializedName("YoungestChildYear")
    @Expose
    private String youngestChildYear;
    @SerializedName("YoungestChildMonth")
    @Expose
    private String youngestChildMonth;
    @SerializedName("MarriedButNoChildYear")
    @Expose
    private String marriedButNoChildYear;
    @SerializedName("MarriedButNoChildMonth")
    @Expose
    private String marriedButNoChildMonth;
    @SerializedName("FSBM01")
    @Expose
    private String fSBM01;
    @SerializedName("FSBM02")
    @Expose
    private String fSBM02;
    @SerializedName("FSBM03")
    @Expose
    private String fSBM03;
    @SerializedName("FSBM04")
    @Expose
    private String fSBM04;
    @SerializedName("FSBM05")
    @Expose
    private String fSBM05;
    @SerializedName("FSBM06")
    @Expose
    private String fSBM06;
    @SerializedName("FSBM07")
    @Expose
    private String fSBM07;
    @SerializedName("FSBM08")
    @Expose
    private String fSBM08;
    @SerializedName("FSBM09")
    @Expose
    private String fSBM09;
    @SerializedName("FSBM10")
    @Expose
    private String fSBM10;
    @SerializedName("FSBM11")
    @Expose
    private String fSBM11;
    @SerializedName("FSBM12")
    @Expose
    private String fSBM12;
    @SerializedName("SheltersId")
    @Expose
    private String sheltersId;
    @SerializedName("Shelter1")
    @Expose
    private String shelter1;
    @SerializedName("Shelter2")
    @Expose
    private String shelter2;
    @SerializedName("Shelter3")
    @Expose
    private String shelter3;
    @SerializedName("ShelterOther1")
    @Expose
    private String shelterOther1;
    @SerializedName("ShelterOther2")
    @Expose
    private String shelterOther2;
    @SerializedName("ShelterOther3")
    @Expose
    private String shelterOther3;
    @SerializedName("FPService")
    @Expose
    private String fPService;
    @SerializedName("SanLatrine")
    @Expose
    private String sanLatrine;
    @SerializedName("HService1")
    @Expose
    private String hService1;
    @SerializedName("HService2")
    @Expose
    private String hService2;
    @SerializedName("HService3")
    @Expose
    private String hService3;
    @SerializedName("DWaterSource")
    @Expose
    private String dWaterSource;
    @SerializedName("TublewellOwner")
    @Expose
    private String tublewellOwner;
    @SerializedName("DWaterTimeHrs")
    @Expose
    private String dWaterTimeHrs;
    @SerializedName("DWaterTimeMin")
    @Expose
    private String dWaterTimeMin;
    @SerializedName("Posibility1")
    @Expose
    private String posibility1;
    @SerializedName("Posibility2")
    @Expose
    private String posibility2;
    @SerializedName("GeoLocation")
    @Expose
    private String geoLocation;
    @SerializedName("GeoLocationOther")
    @Expose
    private String geoLocationOther;
    @SerializedName("FloodAffected")
    @Expose
    private String floodAffected;
    @SerializedName("FloodAffecEveryYr")
    @Expose
    private String floodAffecEveryYr;
    @SerializedName("FloodAffectedTimes")
    @Expose
    private String floodAffectedTimes;
    @SerializedName("HServicesId")
    @Expose
    private String hServicesId;
    @SerializedName("HealthCenterDistance")
    @Expose
    private String healthCenterDistance;
    @SerializedName("HasToilet")
    @Expose
    private String hasToilet;
    @SerializedName("ToiletType")
    @Expose
    private String toiletType;
    @SerializedName("SourceWaterCookingId")
    @Expose
    private String sourceWaterCookingId;
    @SerializedName("SourceWaterOtherId")
    @Expose
    private String sourceWaterOtherId;
    @SerializedName("SourceWaterCookingFt")
    @Expose
    private String sourceWaterCookingFt;
    @SerializedName("SourceWaterOtherFt")
    @Expose
    private String sourceWaterOtherFt;
    @SerializedName("HasHWashFacility")
    @Expose
    private String hasHWashFacility;
    @SerializedName("ChildBirthReg")
    @Expose
    private String childBirthReg;
    @SerializedName("HasBenIncome")
    @Expose
    private String hasBenIncome;
    @SerializedName("IncomeControll")
    @Expose
    private String incomeControll;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getBenID() {
        return benID;
    }

    public void setBenID(String benID) {
        this.benID = benID;
    }

    public String getBenNameEn() {
        return benNameEn;
    }

    public void setBenNameEn(String benNameEn) {
        this.benNameEn = benNameEn;
    }

    public String getBenNameBn() {
        return benNameBn;
    }

    public void setBenNameBn(String benNameBn) {
        this.benNameBn = benNameBn;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getNID() {
        return nID;
    }

    public void setNID(String nID) {
        this.nID = nID;
    }

    public String getIpCode() {
        return ipCode;
    }

    public void setIpCode(String ipCode) {
        this.ipCode = ipCode;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getUpazilaCode() {
        return upazilaCode;
    }

    public void setUpazilaCode(String upazilaCode) {
        this.upazilaCode = upazilaCode;
    }

    public String getUnionCode() {
        return unionCode;
    }

    public void setUnionCode(String unionCode) {
        this.unionCode = unionCode;
    }

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getKhanaCode() {
        return khanaCode;
    }

    public void setKhanaCode(String khanaCode) {
        this.khanaCode = khanaCode;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(String eduLevel) {
        this.eduLevel = eduLevel;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getHasDisabled() {
        return hasDisabled;
    }

    public void setHasDisabled(String hasDisabled) {
        this.hasDisabled = hasDisabled;
    }

    public String getDisablityType() {
        return disablityType;
    }

    public void setDisablityType(String disablityType) {
        this.disablityType = disablityType;
    }

    public String getDisMale() {
        return disMale;
    }

    public void setDisMale(String disMale) {
        this.disMale = disMale;
    }

    public String getDisFemale() {
        return disFemale;
    }

    public void setDisFemale(String disFemale) {
        this.disFemale = disFemale;
    }

    public String getHHHeadName() {
        return hHHeadName;
    }

    public void setHHHeadName(String hHHeadName) {
        this.hHHeadName = hHHeadName;
    }

    public String getHHHeadSex() {
        return hHHeadSex;
    }

    public void setHHHeadSex(String hHHeadSex) {
        this.hHHeadSex = hHHeadSex;
    }

    public String getHasPW() {
        return hasPW;
    }

    public void setHasPW(String hasPW) {
        this.hasPW = hasPW;
    }

    public String getPWMonths() {
        return pWMonths;
    }

    public void setPWMonths(String pWMonths) {
        this.pWMonths = pWMonths;
    }

    public String getCU2M() {
        return cU2M;
    }

    public void setCU2M(String cU2M) {
        this.cU2M = cU2M;
    }

    public String getCU2F() {
        return cU2F;
    }

    public void setCU2F(String cU2F) {
        this.cU2F = cU2F;
    }

    public String getCU2MDOB1() {
        return cU2MDOB1;
    }

    public void setCU2MDOB1(String cU2MDOB1) {
        this.cU2MDOB1 = cU2MDOB1;
    }

    public String getCU2MDOB1Text() {
        return cU2MDOB1Text;
    }

    public void setCU2MDOB1Text(String cU2MDOB1Text) {
        this.cU2MDOB1Text = cU2MDOB1Text;
    }

    public String getCU2MDOB2() {
        return cU2MDOB2;
    }

    public void setCU2MDOB2(String cU2MDOB2) {
        this.cU2MDOB2 = cU2MDOB2;
    }

    public String getCU2MDOB2Text() {
        return cU2MDOB2Text;
    }

    public void setCU2MDOB2Text(String cU2MDOB2Text) {
        this.cU2MDOB2Text = cU2MDOB2Text;
    }

    public String getCU2MDOB3() {
        return cU2MDOB3;
    }

    public void setCU2MDOB3(String cU2MDOB3) {
        this.cU2MDOB3 = cU2MDOB3;
    }

    public String getCU2MDOB3Text() {
        return cU2MDOB3Text;
    }

    public void setCU2MDOB3Text(String cU2MDOB3Text) {
        this.cU2MDOB3Text = cU2MDOB3Text;
    }

    public String getCU2MDOB4() {
        return cU2MDOB4;
    }

    public void setCU2MDOB4(String cU2MDOB4) {
        this.cU2MDOB4 = cU2MDOB4;
    }

    public String getCU2MDOB4Text() {
        return cU2MDOB4Text;
    }

    public void setCU2MDOB4Text(String cU2MDOB4Text) {
        this.cU2MDOB4Text = cU2MDOB4Text;
    }

    public String getCU2FDOB1() {
        return cU2FDOB1;
    }

    public void setCU2FDOB1(String cU2FDOB1) {
        this.cU2FDOB1 = cU2FDOB1;
    }

    public String getCU2FDOB1Text() {
        return cU2FDOB1Text;
    }

    public void setCU2FDOB1Text(String cU2FDOB1Text) {
        this.cU2FDOB1Text = cU2FDOB1Text;
    }

    public String getCU2FDOB2() {
        return cU2FDOB2;
    }

    public void setCU2FDOB2(String cU2FDOB2) {
        this.cU2FDOB2 = cU2FDOB2;
    }

    public String getCU2FDOB2Text() {
        return cU2FDOB2Text;
    }

    public void setCU2FDOB2Text(String cU2FDOB2Text) {
        this.cU2FDOB2Text = cU2FDOB2Text;
    }

    public String getCU2FDOB3() {
        return cU2FDOB3;
    }

    public void setCU2FDOB3(String cU2FDOB3) {
        this.cU2FDOB3 = cU2FDOB3;
    }

    public String getCU2FDOB3Text() {
        return cU2FDOB3Text;
    }

    public void setCU2FDOB3Text(String cU2FDOB3Text) {
        this.cU2FDOB3Text = cU2FDOB3Text;
    }

    public String getCU2FDOB4() {
        return cU2FDOB4;
    }

    public void setCU2FDOB4(String cU2FDOB4) {
        this.cU2FDOB4 = cU2FDOB4;
    }

    public String getCU2FDOB4Text() {
        return cU2FDOB4Text;
    }

    public void setCU2FDOB4Text(String cU2FDOB4Text) {
        this.cU2FDOB4Text = cU2FDOB4Text;
    }

    public String getCU2DOB1() {
        return cU2DOB1;
    }

    public void setCU2DOB1(String cU2DOB1) {
        this.cU2DOB1 = cU2DOB1;
    }

    public String getCU2DOB2() {
        return cU2DOB2;
    }

    public void setCU2DOB2(String cU2DOB2) {
        this.cU2DOB2 = cU2DOB2;
    }

    public String getCU2DOB3() {
        return cU2DOB3;
    }

    public void setCU2DOB3(String cU2DOB3) {
        this.cU2DOB3 = cU2DOB3;
    }

    public String getCU2DOB4() {
        return cU2DOB4;
    }

    public void setCU2DOB4(String cU2DOB4) {
        this.cU2DOB4 = cU2DOB4;
    }

    public String getCU2DOBSourceM() {
        return cU2DOBSourceM;
    }

    public void setCU2DOBSourceM(String cU2DOBSourceM) {
        this.cU2DOBSourceM = cU2DOBSourceM;
    }

    public String getCU2DOBSourceF() {
        return cU2DOBSourceF;
    }

    public void setCU2DOBSourceF(String cU2DOBSourceF) {
        this.cU2DOBSourceF = cU2DOBSourceF;
    }

    public String getCU2DOBSource() {
        return cU2DOBSource;
    }

    public void setCU2DOBSource(String cU2DOBSource) {
        this.cU2DOBSource = cU2DOBSource;
    }

    public String getC2T5M() {
        return c2T5M;
    }

    public void setC2T5M(String c2T5M) {
        this.c2T5M = c2T5M;
    }

    public String getC2T5F() {
        return c2T5F;
    }

    public void setC2T5F(String c2T5F) {
        this.c2T5F = c2T5F;
    }

    public String getC6T14M() {
        return c6T14M;
    }

    public void setC6T14M(String c6T14M) {
        this.c6T14M = c6T14M;
    }

    public String getC6T14F() {
        return c6T14F;
    }

    public void setC6T14F(String c6T14F) {
        this.c6T14F = c6T14F;
    }

    public String getC15T19M() {
        return c15T19M;
    }

    public void setC15T19M(String c15T19M) {
        this.c15T19M = c15T19M;
    }

    public String getC15T19F() {
        return c15T19F;
    }

    public void setC15T19F(String c15T19F) {
        this.c15T19F = c15T19F;
    }

    public String getSGC6T19M() {
        return sGC6T19M;
    }

    public void setSGC6T19M(String sGC6T19M) {
        this.sGC6T19M = sGC6T19M;
    }

    public String getSGC6T19F() {
        return sGC6T19F;
    }

    public void setSGC6T19F(String sGC6T19F) {
        this.sGC6T19F = sGC6T19F;
    }

    public String getLabC6T14M() {
        return labC6T14M;
    }

    public void setLabC6T14M(String labC6T14M) {
        this.labC6T14M = labC6T14M;
    }

    public String getLabC6T14F() {
        return labC6T14F;
    }

    public void setLabC6T14F(String labC6T14F) {
        this.labC6T14F = labC6T14F;
    }

    public String getLabC6T14Type() {
        return labC6T14Type;
    }

    public void setLabC6T14Type(String labC6T14Type) {
        this.labC6T14Type = labC6T14Type;
    }

    public String getKhanaCodeWr() {
        return khanaCodeWr;
    }

    public void setKhanaCodeWr(String khanaCodeWr) {
        this.khanaCodeWr = khanaCodeWr;
    }

    public String getWRankNo() {
        return wRankNo;
    }

    public void setWRankNo(String wRankNo) {
        this.wRankNo = wRankNo;
    }

    public String getWardNo() {
        return wardNo;
    }

    public void setWardNo(String wardNo) {
        this.wardNo = wardNo;
    }

    public String getFatName() {
        return fatName;
    }

    public void setFatName(String fatName) {
        this.fatName = fatName;
    }

    public String getFatType() {
        return fatType;
    }

    public void setFatType(String fatType) {
        this.fatType = fatType;
    }

    public String getMotName() {
        return motName;
    }

    public void setMotName(String motName) {
        this.motName = motName;
    }

    public String getMotType() {
        return motType;
    }

    public void setMotType(String motType) {
        this.motType = motType;
    }

    public String getHusName() {
        return husName;
    }

    public void setHusName(String husName) {
        this.husName = husName;
    }

    public String getAsignOrg() {
        return asignOrg;
    }

    public void setAsignOrg(String asignOrg) {
        this.asignOrg = asignOrg;
    }

    public String getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(String verificationDate) {
        this.verificationDate = verificationDate;
    }

    public String getVerificationDateText() {
        return verificationDateText;
    }

    public void setVerificationDateText(String verificationDateText) {
        this.verificationDateText = verificationDateText;
    }

    public String getAsignOrgOthers() {
        return asignOrgOthers;
    }

    public void setAsignOrgOthers(String asignOrgOthers) {
        this.asignOrgOthers = asignOrgOthers;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getDataColYear() {
        return dataColYear;
    }

    public void setDataColYear(String dataColYear) {
        this.dataColYear = dataColYear;
    }

    public String getDataEnum() {
        return dataEnum;
    }

    public void setDataEnum(String dataEnum) {
        this.dataEnum = dataEnum;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getCellNeig() {
        return cellNeig;
    }

    public void setCellNeig(String cellNeig) {
        this.cellNeig = cellNeig;
    }

    public String getHHMemberMale() {
        return hHMemberMale;
    }

    public void setHHMemberMale(String hHMemberMale) {
        this.hHMemberMale = hHMemberMale;
    }

    public String getHHMemberFemale() {
        return hHMemberFemale;
    }

    public void setHHMemberFemale(String hHMemberFemale) {
        this.hHMemberFemale = hHMemberFemale;
    }

    public String getHHMemberBoth() {
        return hHMemberBoth;
    }

    public void setHHMemberBoth(String hHMemberBoth) {
        this.hHMemberBoth = hHMemberBoth;
    }

    public String getTempID() {
        return tempID;
    }

    public void setTempID(String tempID) {
        this.tempID = tempID;
    }

    public String getInfoProvider() {
        return infoProvider;
    }

    public void setInfoProvider(String infoProvider) {
        this.infoProvider = infoProvider;
    }

    public String getInfoProviderRel() {
        return infoProviderRel;
    }

    public void setInfoProviderRel(String infoProviderRel) {
        this.infoProviderRel = infoProviderRel;
    }

    public String getBenDOB() {
        return benDOB;
    }

    public void setBenDOB(String benDOB) {
        this.benDOB = benDOB;
    }

    public String getIsRecommendedForDeletion() {
        return isRecommendedForDeletion;
    }

    public void setIsRecommendedForDeletion(String isRecommendedForDeletion) {
        this.isRecommendedForDeletion = isRecommendedForDeletion;
    }

    public String getReasonForDeletion() {
        return reasonForDeletion;
    }

    public void setReasonForDeletion(String reasonForDeletion) {
        this.reasonForDeletion = reasonForDeletion;
    }

    public String getReasonDeleteOther() {
        return reasonDeleteOther;
    }

    public void setReasonDeleteOther(String reasonDeleteOther) {
        this.reasonDeleteOther = reasonDeleteOther;
    }

    public String getReasonForDropout() {
        return reasonForDropout;
    }

    public void setReasonForDropout(String reasonForDropout) {
        this.reasonForDropout = reasonForDropout;
    }

    public String getReasonDropOutOther() {
        return reasonDropOutOther;
    }

    public void setReasonDropOutOther(String reasonDropOutOther) {
        this.reasonDropOutOther = reasonDropOutOther;
    }

    public String getSpecialComments() {
        return specialComments;
    }

    public void setSpecialComments(String specialComments) {
        this.specialComments = specialComments;
    }

    public String getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(String verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public String getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(String verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public String getIsDropout() {
        return isDropout;
    }

    public void setIsDropout(String isDropout) {
        this.isDropout = isDropout;
    }

    public String getIsReasonForDropout() {
        return isReasonForDropout;
    }

    public void setIsReasonForDropout(String isReasonForDropout) {
        this.isReasonForDropout = isReasonForDropout;
    }

    public String getHFPSubgroup() {
        return hFPSubgroup;
    }

    public void setHFPSubgroup(String hFPSubgroup) {
        this.hFPSubgroup = hFPSubgroup;
    }

    public String getIGAAssigned() {
        return iGAAssigned;
    }

    public void setIGAAssigned(String iGAAssigned) {
        this.iGAAssigned = iGAAssigned;
    }

    public String getSGA() {
        return sGA;
    }

    public void setSGA(String sGA) {
        this.sGA = sGA;
    }

    public String getSGM() {
        return sGM;
    }

    public void setSGM(String sGM) {
        this.sGM = sGM;
    }

    public String getSGSA() {
        return sGSA;
    }

    public void setSGSA(String sGSA) {
        this.sGSA = sGSA;
    }

    public String getSGSW() {
        return sGSW;
    }

    public void setSGSW(String sGSW) {
        this.sGSW = sGSW;
    }

    public String getDV() {
        return dV;
    }

    public void setDV(String dV) {
        this.dV = dV;
    }

    public String getVerifiedBenNameEn() {
        return verifiedBenNameEn;
    }

    public void setVerifiedBenNameEn(String verifiedBenNameEn) {
        this.verifiedBenNameEn = verifiedBenNameEn;
    }

    public String getNameChangedReason() {
        return nameChangedReason;
    }

    public void setNameChangedReason(String nameChangedReason) {
        this.nameChangedReason = nameChangedReason;
    }

    public String getEarnerMale() {
        return earnerMale;
    }

    public void setEarnerMale(String earnerMale) {
        this.earnerMale = earnerMale;
    }

    public String getEarnerFemale() {
        return earnerFemale;
    }

    public void setEarnerFemale(String earnerFemale) {
        this.earnerFemale = earnerFemale;
    }

    public String getPrimeEarner() {
        return primeEarner;
    }

    public void setPrimeEarner(String primeEarner) {
        this.primeEarner = primeEarner;
    }

    public String getPrimeEarnerTxt() {
        return primeEarnerTxt;
    }

    public void setPrimeEarnerTxt(String primeEarnerTxt) {
        this.primeEarnerTxt = primeEarnerTxt;
    }

    public String getMoreEarnerNo() {
        return moreEarnerNo;
    }

    public void setMoreEarnerNo(String moreEarnerNo) {
        this.moreEarnerNo = moreEarnerNo;
    }

    public String getMoreEarnerName() {
        return moreEarnerName;
    }

    public void setMoreEarnerName(String moreEarnerName) {
        this.moreEarnerName = moreEarnerName;
    }

    public String getHasMoreEarner() {
        return hasMoreEarner;
    }

    public void setHasMoreEarner(String hasMoreEarner) {
        this.hasMoreEarner = hasMoreEarner;
    }

    public String getPIncomeMonth() {
        return pIncomeMonth;
    }

    public void setPIncomeMonth(String pIncomeMonth) {
        this.pIncomeMonth = pIncomeMonth;
    }

    public String getPIncomeDomain() {
        return pIncomeDomain;
    }

    public void setPIncomeDomain(String pIncomeDomain) {
        this.pIncomeDomain = pIncomeDomain;
    }

    public String getPIncomeOtr() {
        return pIncomeOtr;
    }

    public void setPIncomeOtr(String pIncomeOtr) {
        this.pIncomeOtr = pIncomeOtr;
    }

    public String getPIncomeOtrTxt() {
        return pIncomeOtrTxt;
    }

    public void setPIncomeOtrTxt(String pIncomeOtrTxt) {
        this.pIncomeOtrTxt = pIncomeOtrTxt;
    }

    public String getMIncomeHas() {
        return mIncomeHas;
    }

    public void setMIncomeHas(String mIncomeHas) {
        this.mIncomeHas = mIncomeHas;
    }

    public String getMIncomeDomain() {
        return mIncomeDomain;
    }

    public void setMIncomeDomain(String mIncomeDomain) {
        this.mIncomeDomain = mIncomeDomain;
    }

    public String getMIncomeOtr() {
        return mIncomeOtr;
    }

    public void setMIncomeOtr(String mIncomeOtr) {
        this.mIncomeOtr = mIncomeOtr;
    }

    public String getPIncomeTaka() {
        return pIncomeTaka;
    }

    public void setPIncomeTaka(String pIncomeTaka) {
        this.pIncomeTaka = pIncomeTaka;
    }

    public String getMIncomeTaka() {
        return mIncomeTaka;
    }

    public void setMIncomeTaka(String mIncomeTaka) {
        this.mIncomeTaka = mIncomeTaka;
    }

    public String getHasSsp() {
        return hasSsp;
    }

    public void setHasSsp(String hasSsp) {
        this.hasSsp = hasSsp;
    }

    public String getSspCode() {
        return sspCode;
    }

    public void setSspCode(String sspCode) {
        this.sspCode = sspCode;
    }

    public String getSspOthers() {
        return sspOthers;
    }

    public void setSspOthers(String sspOthers) {
        this.sspOthers = sspOthers;
    }

    public String getHasBankAc() {
        return hasBankAc;
    }

    public void setHasBankAc(String hasBankAc) {
        this.hasBankAc = hasBankAc;
    }

    public String getHasLoan() {
        return hasLoan;
    }

    public void setHasLoan(String hasLoan) {
        this.hasLoan = hasLoan;
    }

    public String getLoanSrc1() {
        return loanSrc1;
    }

    public void setLoanSrc1(String loanSrc1) {
        this.loanSrc1 = loanSrc1;
    }

    public String getLoanSrc2() {
        return loanSrc2;
    }

    public void setLoanSrc2(String loanSrc2) {
        this.loanSrc2 = loanSrc2;
    }

    public String getLoanSrc3() {
        return loanSrc3;
    }

    public void setLoanSrc3(String loanSrc3) {
        this.loanSrc3 = loanSrc3;
    }

    public String getLoanUse1() {
        return loanUse1;
    }

    public void setLoanUse1(String loanUse1) {
        this.loanUse1 = loanUse1;
    }

    public String getLoanUse2() {
        return loanUse2;
    }

    public void setLoanUse2(String loanUse2) {
        this.loanUse2 = loanUse2;
    }

    public String getLoanUse3() {
        return loanUse3;
    }

    public void setLoanUse3(String loanUse3) {
        this.loanUse3 = loanUse3;
    }

    public String getLoanUseOtr() {
        return loanUseOtr;
    }

    public void setLoanUseOtr(String loanUseOtr) {
        this.loanUseOtr = loanUseOtr;
    }

    public String getLoanIntRate() {
        return loanIntRate;
    }

    public void setLoanIntRate(String loanIntRate) {
        this.loanIntRate = loanIntRate;
    }

    public String getLoanIntTaka() {
        return loanIntTaka;
    }

    public void setLoanIntTaka(String loanIntTaka) {
        this.loanIntTaka = loanIntTaka;
    }

    public String getLoanRepay1() {
        return loanRepay1;
    }

    public void setLoanRepay1(String loanRepay1) {
        this.loanRepay1 = loanRepay1;
    }

    public String getLoanRepay2() {
        return loanRepay2;
    }

    public void setLoanRepay2(String loanRepay2) {
        this.loanRepay2 = loanRepay2;
    }

    public String getLoanRepay3() {
        return loanRepay3;
    }

    public void setLoanRepay3(String loanRepay3) {
        this.loanRepay3 = loanRepay3;
    }

    public String getLoanRepayOtr() {
        return loanRepayOtr;
    }

    public void setLoanRepayOtr(String loanRepayOtr) {
        this.loanRepayOtr = loanRepayOtr;
    }

    public String getHasSavings() {
        return hasSavings;
    }

    public void setHasSavings(String hasSavings) {
        this.hasSavings = hasSavings;
    }

    public String getSaveingsOrg1() {
        return saveingsOrg1;
    }

    public void setSaveingsOrg1(String saveingsOrg1) {
        this.saveingsOrg1 = saveingsOrg1;
    }

    public String getSaveingsOrg2() {
        return saveingsOrg2;
    }

    public void setSaveingsOrg2(String saveingsOrg2) {
        this.saveingsOrg2 = saveingsOrg2;
    }

    public String getSaveingsOrg3() {
        return saveingsOrg3;
    }

    public void setSaveingsOrg3(String saveingsOrg3) {
        this.saveingsOrg3 = saveingsOrg3;
    }

    public String getSaveingsOrgOtrs() {
        return saveingsOrgOtrs;
    }

    public void setSaveingsOrgOtrs(String saveingsOrgOtrs) {
        this.saveingsOrgOtrs = saveingsOrgOtrs;
    }

    public String getHsLandYn() {
        return hsLandYn;
    }

    public void setHsLandYn(String hsLandYn) {
        this.hsLandYn = hsLandYn;
    }

    public String getHsLandDec() {
        return hsLandDec;
    }

    public void setHsLandDec(String hsLandDec) {
        this.hsLandDec = hsLandDec;
    }

    public String getCuLandYn() {
        return cuLandYn;
    }

    public void setCuLandYn(String cuLandYn) {
        this.cuLandYn = cuLandYn;
    }

    public String getCuLandDec() {
        return cuLandDec;
    }

    public void setCuLandDec(String cuLandDec) {
        this.cuLandDec = cuLandDec;
    }

    public String getVegFrtYn() {
        return vegFrtYn;
    }

    public void setVegFrtYn(String vegFrtYn) {
        this.vegFrtYn = vegFrtYn;
    }

    public String getVegFrtType() {
        return vegFrtType;
    }

    public void setVegFrtType(String vegFrtType) {
        this.vegFrtType = vegFrtType;
    }

    public String getDukCknYn() {
        return dukCknYn;
    }

    public void setDukCknYn(String dukCknYn) {
        this.dukCknYn = dukCknYn;
    }

    public String getCattleYn() {
        return cattleYn;
    }

    public void setCattleYn(String cattleYn) {
        this.cattleYn = cattleYn;
    }

    public String getGoatSheepYn() {
        return goatSheepYn;
    }

    public void setGoatSheepYn(String goatSheepYn) {
        this.goatSheepYn = goatSheepYn;
    }

    public String getHfpUtil() {
        return hfpUtil;
    }

    public void setHfpUtil(String hfpUtil) {
        this.hfpUtil = hfpUtil;
    }

    public String getPondYn() {
        return pondYn;
    }

    public void setPondYn(String pondYn) {
        this.pondYn = pondYn;
    }

    public String getPondDec() {
        return pondDec;
    }

    public void setPondDec(String pondDec) {
        this.pondDec = pondDec;
    }

    public String getPondType() {
        return pondType;
    }

    public void setPondType(String pondType) {
        this.pondType = pondType;
    }

    public String getPondWaterMonth() {
        return pondWaterMonth;
    }

    public void setPondWaterMonth(String pondWaterMonth) {
        this.pondWaterMonth = pondWaterMonth;
    }

    public String getPondOwnership() {
        return pondOwnership;
    }

    public void setPondOwnership(String pondOwnership) {
        this.pondOwnership = pondOwnership;
    }

    public String getPondShareHolders() {
        return pondShareHolders;
    }

    public void setPondShareHolders(String pondShareHolders) {
        this.pondShareHolders = pondShareHolders;
    }

    public String getAccessLakeYn() {
        return accessLakeYn;
    }

    public void setAccessLakeYn(String accessLakeYn) {
        this.accessLakeYn = accessLakeYn;
    }

    public String getLakeType() {
        return lakeType;
    }

    public void setLakeType(String lakeType) {
        this.lakeType = lakeType;
    }

    public String getLakeShareHolders() {
        return lakeShareHolders;
    }

    public void setLakeShareHolders(String lakeShareHolders) {
        this.lakeShareHolders = lakeShareHolders;
    }

    public String getLakeTypeOtr() {
        return lakeTypeOtr;
    }

    public void setLakeTypeOtr(String lakeTypeOtr) {
        this.lakeTypeOtr = lakeTypeOtr;
    }

    public String getFCulYn() {
        return fCulYn;
    }

    public void setFCulYn(String fCulYn) {
        this.fCulYn = fCulYn;
    }

    public String getFCulType() {
        return fCulType;
    }

    public void setFCulType(String fCulType) {
        this.fCulType = fCulType;
    }

    public String getFCulTypeOtr() {
        return fCulTypeOtr;
    }

    public void setFCulTypeOtr(String fCulTypeOtr) {
        this.fCulTypeOtr = fCulTypeOtr;
    }

    public String getFCulNotType1() {
        return fCulNotType1;
    }

    public void setFCulNotType1(String fCulNotType1) {
        this.fCulNotType1 = fCulNotType1;
    }

    public String getFCulNotType2() {
        return fCulNotType2;
    }

    public void setFCulNotType2(String fCulNotType2) {
        this.fCulNotType2 = fCulNotType2;
    }

    public String getFCulNotTypeOtr() {
        return fCulNotTypeOtr;
    }

    public void setFCulNotTypeOtr(String fCulNotTypeOtr) {
        this.fCulNotTypeOtr = fCulNotTypeOtr;
    }

    public String getFBusYn() {
        return fBusYn;
    }

    public void setFBusYn(String fBusYn) {
        this.fBusYn = fBusYn;
    }

    public String getFBusType() {
        return fBusType;
    }

    public void setFBusType(String fBusType) {
        this.fBusType = fBusType;
    }

    public String getFBusOtr() {
        return fBusOtr;
    }

    public void setFBusOtr(String fBusOtr) {
        this.fBusOtr = fBusOtr;
    }

    public String getFProdDryYn() {
        return fProdDryYn;
    }

    public void setFProdDryYn(String fProdDryYn) {
        this.fProdDryYn = fProdDryYn;
    }

    public String getFProdDryUse() {
        return fProdDryUse;
    }

    public void setFProdDryUse(String fProdDryUse) {
        this.fProdDryUse = fProdDryUse;
    }

    public String getBusExpBenYn() {
        return busExpBenYn;
    }

    public void setBusExpBenYn(String busExpBenYn) {
        this.busExpBenYn = busExpBenYn;
    }

    public String getBusExpMemYn() {
        return busExpMemYn;
    }

    public void setBusExpMemYn(String busExpMemYn) {
        this.busExpMemYn = busExpMemYn;
    }

    public String getTrainingYn() {
        return trainingYn;
    }

    public void setTrainingYn(String trainingYn) {
        this.trainingYn = trainingYn;
    }

    public String getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }

    public String getTrainingTypeOtr() {
        return trainingTypeOtr;
    }

    public void setTrainingTypeOtr(String trainingTypeOtr) {
        this.trainingTypeOtr = trainingTypeOtr;
    }

    public String getDoingBusYn() {
        return doingBusYn;
    }

    public void setDoingBusYn(String doingBusYn) {
        this.doingBusYn = doingBusYn;
    }

    public String getDoingBusType() {
        return doingBusType;
    }

    public void setDoingBusType(String doingBusType) {
        this.doingBusType = doingBusType;
    }

    public String getPoverty() {
        return poverty;
    }

    public void setPoverty(String poverty) {
        this.poverty = poverty;
    }

    public String getVPAssetCow() {
        return vPAssetCow;
    }

    public void setVPAssetCow(String vPAssetCow) {
        this.vPAssetCow = vPAssetCow;
    }

    public String getVPAssetPoultry() {
        return vPAssetPoultry;
    }

    public void setVPAssetPoultry(String vPAssetPoultry) {
        this.vPAssetPoultry = vPAssetPoultry;
    }

    public String getVPAssetRickshaw() {
        return vPAssetRickshaw;
    }

    public void setVPAssetRickshaw(String vPAssetRickshaw) {
        this.vPAssetRickshaw = vPAssetRickshaw;
    }

    public String getVPAssetSewingMac() {
        return vPAssetSewingMac;
    }

    public void setVPAssetSewingMac(String vPAssetSewingMac) {
        this.vPAssetSewingMac = vPAssetSewingMac;
    }

    public String getVPAssetBicycle() {
        return vPAssetBicycle;
    }

    public void setVPAssetBicycle(String vPAssetBicycle) {
        this.vPAssetBicycle = vPAssetBicycle;
    }

    public String getVPAssetPettyBus() {
        return vPAssetPettyBus;
    }

    public void setVPAssetPettyBus(String vPAssetPettyBus) {
        this.vPAssetPettyBus = vPAssetPettyBus;
    }

    public String getVPAssetNursery() {
        return vPAssetNursery;
    }

    public void setVPAssetNursery(String vPAssetNursery) {
        this.vPAssetNursery = vPAssetNursery;
    }

    public String getVPAssetBusiness() {
        return vPAssetBusiness;
    }

    public void setVPAssetBusiness(String vPAssetBusiness) {
        this.vPAssetBusiness = vPAssetBusiness;
    }

    public String getVPAssetGoat() {
        return vPAssetGoat;
    }

    public void setVPAssetGoat(String vPAssetGoat) {
        this.vPAssetGoat = vPAssetGoat;
    }

    public String getVPAssetBoat() {
        return vPAssetBoat;
    }

    public void setVPAssetBoat(String vPAssetBoat) {
        this.vPAssetBoat = vPAssetBoat;
    }

    public String getVPAssetFfishnet() {
        return vPAssetFfishnet;
    }

    public void setVPAssetFfishnet(String vPAssetFfishnet) {
        this.vPAssetFfishnet = vPAssetFfishnet;
    }

    public String getVPAssetSavings() {
        return vPAssetSavings;
    }

    public void setVPAssetSavings(String vPAssetSavings) {
        this.vPAssetSavings = vPAssetSavings;
    }

    public String getVPAssetMobile() {
        return vPAssetMobile;
    }

    public void setVPAssetMobile(String vPAssetMobile) {
        this.vPAssetMobile = vPAssetMobile;
    }

    public String getVPAssetMCycle() {
        return vPAssetMCycle;
    }

    public void setVPAssetMCycle(String vPAssetMCycle) {
        this.vPAssetMCycle = vPAssetMCycle;
    }

    public String getVPAssetOtr1() {
        return vPAssetOtr1;
    }

    public void setVPAssetOtr1(String vPAssetOtr1) {
        this.vPAssetOtr1 = vPAssetOtr1;
    }

    public String getVPAssetOtr1num() {
        return vPAssetOtr1num;
    }

    public void setVPAssetOtr1num(String vPAssetOtr1num) {
        this.vPAssetOtr1num = vPAssetOtr1num;
    }

    public String getVPAssetOtr2() {
        return vPAssetOtr2;
    }

    public void setVPAssetOtr2(String vPAssetOtr2) {
        this.vPAssetOtr2 = vPAssetOtr2;
    }

    public String getVPAssetOtr2num() {
        return vPAssetOtr2num;
    }

    public void setVPAssetOtr2num(String vPAssetOtr2num) {
        this.vPAssetOtr2num = vPAssetOtr2num;
    }

    public String getVPAssetOtr3() {
        return vPAssetOtr3;
    }

    public void setVPAssetOtr3(String vPAssetOtr3) {
        this.vPAssetOtr3 = vPAssetOtr3;
    }

    public String getVPAssetOtr3num() {
        return vPAssetOtr3num;
    }

    public void setVPAssetOtr3num(String vPAssetOtr3num) {
        this.vPAssetOtr3num = vPAssetOtr3num;
    }

    public String getProductiveAssetsId() {
        return productiveAssetsId;
    }

    public void setProductiveAssetsId(String productiveAssetsId) {
        this.productiveAssetsId = productiveAssetsId;
    }

    public String getMaleAttendance() {
        return maleAttendance;
    }

    public void setMaleAttendance(String maleAttendance) {
        this.maleAttendance = maleAttendance;
    }

    public String getFemaleAttendance() {
        return femaleAttendance;
    }

    public void setFemaleAttendance(String femaleAttendance) {
        this.femaleAttendance = femaleAttendance;
    }

    public String getTotalAttendance() {
        return totalAttendance;
    }

    public void setTotalAttendance(String totalAttendance) {
        this.totalAttendance = totalAttendance;
    }

    public String getKhanaMemberComments() {
        return khanaMemberComments;
    }

    public void setKhanaMemberComments(String khanaMemberComments) {
        this.khanaMemberComments = khanaMemberComments;
    }

    public String getHhType() {
        return hhType;
    }

    public void setHhType(String hhType) {
        this.hhType = hhType;
    }

    public String getBenType() {
        return benType;
    }

    public void setBenType(String benType) {
        this.benType = benType;
    }

    public String getPregnantOrLactating() {
        return pregnantOrLactating;
    }

    public void setPregnantOrLactating(String pregnantOrLactating) {
        this.pregnantOrLactating = pregnantOrLactating;
    }

    public String getLandOwnershipType() {
        return landOwnershipType;
    }

    public void setLandOwnershipType(String landOwnershipType) {
        this.landOwnershipType = landOwnershipType;
    }

    public String getRoof() {
        return roof;
    }

    public void setRoof(String roof) {
        this.roof = roof;
    }

    public String getAroundOfHouse() {
        return aroundOfHouse;
    }

    public void setAroundOfHouse(String aroundOfHouse) {
        this.aroundOfHouse = aroundOfHouse;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getLiveMode() {
        return liveMode;
    }

    public void setLiveMode(String liveMode) {
        this.liveMode = liveMode;
    }

    public String getLiveYears() {
        return liveYears;
    }

    public void setLiveYears(String liveYears) {
        this.liveYears = liveYears;
    }

    public String getHasMobileBankingAc() {
        return hasMobileBankingAc;
    }

    public void setHasMobileBankingAc(String hasMobileBankingAc) {
        this.hasMobileBankingAc = hasMobileBankingAc;
    }

    public String getSaveingsPerson() {
        return saveingsPerson;
    }

    public void setSaveingsPerson(String saveingsPerson) {
        this.saveingsPerson = saveingsPerson;
    }

    public String getVPAssetBuffalo() {
        return vPAssetBuffalo;
    }

    public void setVPAssetBuffalo(String vPAssetBuffalo) {
        this.vPAssetBuffalo = vPAssetBuffalo;
    }

    public String getVPAssetSheep() {
        return vPAssetSheep;
    }

    public void setVPAssetSheep(String vPAssetSheep) {
        this.vPAssetSheep = vPAssetSheep;
    }

    public String getVPAssetDuck() {
        return vPAssetDuck;
    }

    public void setVPAssetDuck(String vPAssetDuck) {
        this.vPAssetDuck = vPAssetDuck;
    }

    public String getVPAssetLivestockOthers() {
        return vPAssetLivestockOthers;
    }

    public void setVPAssetLivestockOthers(String vPAssetLivestockOthers) {
        this.vPAssetLivestockOthers = vPAssetLivestockOthers;
    }

    public String getVPAssetTv() {
        return vPAssetTv;
    }

    public void setVPAssetTv(String vPAssetTv) {
        this.vPAssetTv = vPAssetTv;
    }

    public String getVPAssetOtherFoodItems() {
        return vPAssetOtherFoodItems;
    }

    public void setVPAssetOtherFoodItems(String vPAssetOtherFoodItems) {
        this.vPAssetOtherFoodItems = vPAssetOtherFoodItems;
    }

    public String getVPAssetOrnaments() {
        return vPAssetOrnaments;
    }

    public void setVPAssetOrnaments(String vPAssetOrnaments) {
        this.vPAssetOrnaments = vPAssetOrnaments;
    }

    public String getVPAssetLargeTrees() {
        return vPAssetLargeTrees;
    }

    public void setVPAssetLargeTrees(String vPAssetLargeTrees) {
        this.vPAssetLargeTrees = vPAssetLargeTrees;
    }

    public String getVPAssetTubewell() {
        return vPAssetTubewell;
    }

    public void setVPAssetTubewell(String vPAssetTubewell) {
        this.vPAssetTubewell = vPAssetTubewell;
    }

    public String getVPAssetOthers() {
        return vPAssetOthers;
    }

    public void setVPAssetOthers(String vPAssetOthers) {
        this.vPAssetOthers = vPAssetOthers;
    }

    public String getDukCknUtil() {
        return dukCknUtil;
    }

    public void setDukCknUtil(String dukCknUtil) {
        this.dukCknUtil = dukCknUtil;
    }

    public String getFloodAffectdPondMonth() {
        return floodAffectdPondMonth;
    }

    public void setFloodAffectdPondMonth(String floodAffectdPondMonth) {
        this.floodAffectdPondMonth = floodAffectdPondMonth;
    }

    public String getGovernmentAgency() {
        return governmentAgency;
    }

    public void setGovernmentAgency(String governmentAgency) {
        this.governmentAgency = governmentAgency;
    }

    public String getPrivateAgency() {
        return privateAgency;
    }

    public void setPrivateAgency(String privateAgency) {
        this.privateAgency = privateAgency;
    }

    public String getYoungestChildYear() {
        return youngestChildYear;
    }

    public void setYoungestChildYear(String youngestChildYear) {
        this.youngestChildYear = youngestChildYear;
    }

    public String getYoungestChildMonth() {
        return youngestChildMonth;
    }

    public void setYoungestChildMonth(String youngestChildMonth) {
        this.youngestChildMonth = youngestChildMonth;
    }

    public String getMarriedButNoChildYear() {
        return marriedButNoChildYear;
    }

    public void setMarriedButNoChildYear(String marriedButNoChildYear) {
        this.marriedButNoChildYear = marriedButNoChildYear;
    }

    public String getMarriedButNoChildMonth() {
        return marriedButNoChildMonth;
    }

    public void setMarriedButNoChildMonth(String marriedButNoChildMonth) {
        this.marriedButNoChildMonth = marriedButNoChildMonth;
    }

    public String getFSBM01() {
        return fSBM01;
    }

    public void setFSBM01(String fSBM01) {
        this.fSBM01 = fSBM01;
    }

    public String getFSBM02() {
        return fSBM02;
    }

    public void setFSBM02(String fSBM02) {
        this.fSBM02 = fSBM02;
    }

    public String getFSBM03() {
        return fSBM03;
    }

    public void setFSBM03(String fSBM03) {
        this.fSBM03 = fSBM03;
    }

    public String getFSBM04() {
        return fSBM04;
    }

    public void setFSBM04(String fSBM04) {
        this.fSBM04 = fSBM04;
    }

    public String getFSBM05() {
        return fSBM05;
    }

    public void setFSBM05(String fSBM05) {
        this.fSBM05 = fSBM05;
    }

    public String getFSBM06() {
        return fSBM06;
    }

    public void setFSBM06(String fSBM06) {
        this.fSBM06 = fSBM06;
    }

    public String getFSBM07() {
        return fSBM07;
    }

    public void setFSBM07(String fSBM07) {
        this.fSBM07 = fSBM07;
    }

    public String getFSBM08() {
        return fSBM08;
    }

    public void setFSBM08(String fSBM08) {
        this.fSBM08 = fSBM08;
    }

    public String getFSBM09() {
        return fSBM09;
    }

    public void setFSBM09(String fSBM09) {
        this.fSBM09 = fSBM09;
    }

    public String getFSBM10() {
        return fSBM10;
    }

    public void setFSBM10(String fSBM10) {
        this.fSBM10 = fSBM10;
    }

    public String getFSBM11() {
        return fSBM11;
    }

    public void setFSBM11(String fSBM11) {
        this.fSBM11 = fSBM11;
    }

    public String getFSBM12() {
        return fSBM12;
    }

    public void setFSBM12(String fSBM12) {
        this.fSBM12 = fSBM12;
    }

    public String getSheltersId() {
        return sheltersId;
    }

    public void setSheltersId(String sheltersId) {
        this.sheltersId = sheltersId;
    }

    public String getShelter1() {
        return shelter1;
    }

    public void setShelter1(String shelter1) {
        this.shelter1 = shelter1;
    }

    public String getShelter2() {
        return shelter2;
    }

    public void setShelter2(String shelter2) {
        this.shelter2 = shelter2;
    }

    public String getShelter3() {
        return shelter3;
    }

    public void setShelter3(String shelter3) {
        this.shelter3 = shelter3;
    }

    public String getShelterOther1() {
        return shelterOther1;
    }

    public void setShelterOther1(String shelterOther1) {
        this.shelterOther1 = shelterOther1;
    }

    public String getShelterOther2() {
        return shelterOther2;
    }

    public void setShelterOther2(String shelterOther2) {
        this.shelterOther2 = shelterOther2;
    }

    public String getShelterOther3() {
        return shelterOther3;
    }

    public void setShelterOther3(String shelterOther3) {
        this.shelterOther3 = shelterOther3;
    }

    public String getFPService() {
        return fPService;
    }

    public void setFPService(String fPService) {
        this.fPService = fPService;
    }

    public String getSanLatrine() {
        return sanLatrine;
    }

    public void setSanLatrine(String sanLatrine) {
        this.sanLatrine = sanLatrine;
    }

    public String getHService1() {
        return hService1;
    }

    public void setHService1(String hService1) {
        this.hService1 = hService1;
    }

    public String getHService2() {
        return hService2;
    }

    public void setHService2(String hService2) {
        this.hService2 = hService2;
    }

    public String getHService3() {
        return hService3;
    }

    public void setHService3(String hService3) {
        this.hService3 = hService3;
    }

    public String getDWaterSource() {
        return dWaterSource;
    }

    public void setDWaterSource(String dWaterSource) {
        this.dWaterSource = dWaterSource;
    }

    public String getTublewellOwner() {
        return tublewellOwner;
    }

    public void setTublewellOwner(String tublewellOwner) {
        this.tublewellOwner = tublewellOwner;
    }

    public String getDWaterTimeHrs() {
        return dWaterTimeHrs;
    }

    public void setDWaterTimeHrs(String dWaterTimeHrs) {
        this.dWaterTimeHrs = dWaterTimeHrs;
    }

    public String getDWaterTimeMin() {
        return dWaterTimeMin;
    }

    public void setDWaterTimeMin(String dWaterTimeMin) {
        this.dWaterTimeMin = dWaterTimeMin;
    }

    public String getPosibility1() {
        return posibility1;
    }

    public void setPosibility1(String posibility1) {
        this.posibility1 = posibility1;
    }

    public String getPosibility2() {
        return posibility2;
    }

    public void setPosibility2(String posibility2) {
        this.posibility2 = posibility2;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getGeoLocationOther() {
        return geoLocationOther;
    }

    public void setGeoLocationOther(String geoLocationOther) {
        this.geoLocationOther = geoLocationOther;
    }

    public String getFloodAffected() {
        return floodAffected;
    }

    public void setFloodAffected(String floodAffected) {
        this.floodAffected = floodAffected;
    }

    public String getFloodAffecEveryYr() {
        return floodAffecEveryYr;
    }

    public void setFloodAffecEveryYr(String floodAffecEveryYr) {
        this.floodAffecEveryYr = floodAffecEveryYr;
    }

    public String getFloodAffectedTimes() {
        return floodAffectedTimes;
    }

    public void setFloodAffectedTimes(String floodAffectedTimes) {
        this.floodAffectedTimes = floodAffectedTimes;
    }

    public String getHServicesId() {
        return hServicesId;
    }

    public void setHServicesId(String hServicesId) {
        this.hServicesId = hServicesId;
    }

    public String getHealthCenterDistance() {
        return healthCenterDistance;
    }

    public void setHealthCenterDistance(String healthCenterDistance) {
        this.healthCenterDistance = healthCenterDistance;
    }

    public String getHasToilet() {
        return hasToilet;
    }

    public void setHasToilet(String hasToilet) {
        this.hasToilet = hasToilet;
    }

    public String getToiletType() {
        return toiletType;
    }

    public void setToiletType(String toiletType) {
        this.toiletType = toiletType;
    }

    public String getSourceWaterCookingId() {
        return sourceWaterCookingId;
    }

    public void setSourceWaterCookingId(String sourceWaterCookingId) {
        this.sourceWaterCookingId = sourceWaterCookingId;
    }

    public String getSourceWaterOtherId() {
        return sourceWaterOtherId;
    }

    public void setSourceWaterOtherId(String sourceWaterOtherId) {
        this.sourceWaterOtherId = sourceWaterOtherId;
    }

    public String getSourceWaterCookingFt() {
        return sourceWaterCookingFt;
    }

    public void setSourceWaterCookingFt(String sourceWaterCookingFt) {
        this.sourceWaterCookingFt = sourceWaterCookingFt;
    }

    public String getSourceWaterOtherFt() {
        return sourceWaterOtherFt;
    }

    public void setSourceWaterOtherFt(String sourceWaterOtherFt) {
        this.sourceWaterOtherFt = sourceWaterOtherFt;
    }

    public String getHasHWashFacility() {
        return hasHWashFacility;
    }

    public void setHasHWashFacility(String hasHWashFacility) {
        this.hasHWashFacility = hasHWashFacility;
    }

    public String getChildBirthReg() {
        return childBirthReg;
    }

    public void setChildBirthReg(String childBirthReg) {
        this.childBirthReg = childBirthReg;
    }

    public String getHasBenIncome() {
        return hasBenIncome;
    }

    public void setHasBenIncome(String hasBenIncome) {
        this.hasBenIncome = hasBenIncome;
    }

    public String getIncomeControll() {
        return incomeControll;
    }

    public void setIncomeControll(String incomeControll) {
        this.incomeControll = incomeControll;
    }

}
