package com.example.duti.suchanaregistrationdata.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BenInformation {

    @SerializedName("benAllInfo")
    @Expose
    private List<BenAllInfo> benAllInfo = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<BenAllInfo> getBenAllInfo() {
        return benAllInfo;
    }

    public void setBenAllInfo(List<BenAllInfo> benAllInfo) {
        this.benAllInfo = benAllInfo;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
