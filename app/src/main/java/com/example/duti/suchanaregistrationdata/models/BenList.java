package com.example.duti.suchanaregistrationdata.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BenList {

    @SerializedName("benificiaryFewInfo")
    @Expose
    private List<BenificiaryFewInfo> benificiaryFewInfo = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<BenificiaryFewInfo> getBenificiaryFewInfo() {
        return benificiaryFewInfo;
    }

    public void setBenificiaryFewInfo(List<BenificiaryFewInfo> benificiaryFewInfo) {
        this.benificiaryFewInfo = benificiaryFewInfo;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}