package com.example.duti.suchanaregistrationdata.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BenificiaryFewInfo {

    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("BenID")
    @Expose
    private String benID;
    @SerializedName("BenNameEn")
    @Expose
    private String benNameEn;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("HusName")
    @Expose
    private String husName;
    @SerializedName("HsLandDec")
    @Expose
    private String hsLandDec;
    @SerializedName("PondDec")
    @Expose
    private String pondDec;
    @SerializedName("HhType")
    @Expose
    private String hhType;
    @SerializedName("BenType")
    @Expose
    private String benType;
    @SerializedName("PregnantOrLactating")
    @Expose
    private String pregnantOrLactating;
    @SerializedName("YoungestChildYear")
    @Expose
    private String youngestChildYear;
    @SerializedName("YoungestChildMonth")
    @Expose
    private String youngestChildMonth;
    @SerializedName("MarriedButNoChildYear")
    @Expose
    private String marriedButNoChildYear;
    @SerializedName("MarriedButNoChildMonth")
    @Expose
    private String marriedButNoChildMonth;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getBenID() {
        return benID;
    }

    public void setBenID(String benID) {
        this.benID = benID;
    }

    public String getBenNameEn() {
        return benNameEn;
    }

    public void setBenNameEn(String benNameEn) {
        this.benNameEn = benNameEn;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHusName() {
        return husName;
    }

    public void setHusName(String husName) {
        this.husName = husName;
    }

    public String getHsLandDec() {
        return hsLandDec;
    }

    public void setHsLandDec(String hsLandDec) {
        this.hsLandDec = hsLandDec;
    }

    public String getPondDec() {
        return pondDec;
    }

    public void setPondDec(String pondDec) {
        this.pondDec = pondDec;
    }

    public String getHhType() {
        return hhType;
    }

    public void setHhType(String hhType) {
        this.hhType = hhType;
    }

    public String getBenType() {
        return benType;
    }

    public void setBenType(String benType) {
        this.benType = benType;
    }

    public String getPregnantOrLactating() {
        return pregnantOrLactating;
    }

    public void setPregnantOrLactating(String pregnantOrLactating) {
        this.pregnantOrLactating = pregnantOrLactating;
    }

    public String getYoungestChildYear() {
        return youngestChildYear;
    }

    public void setYoungestChildYear(String youngestChildYear) {
        this.youngestChildYear = youngestChildYear;
    }

    public String getYoungestChildMonth() {
        return youngestChildMonth;
    }

    public void setYoungestChildMonth(String youngestChildMonth) {
        this.youngestChildMonth = youngestChildMonth;
    }

    public String getMarriedButNoChildYear() {
        return marriedButNoChildYear;
    }

    public void setMarriedButNoChildYear(String marriedButNoChildYear) {
        this.marriedButNoChildYear = marriedButNoChildYear;
    }

    public String getMarriedButNoChildMonth() {
        return marriedButNoChildMonth;
    }

    public void setMarriedButNoChildMonth(String marriedButNoChildMonth) {
        this.marriedButNoChildMonth = marriedButNoChildMonth;
    }

}
