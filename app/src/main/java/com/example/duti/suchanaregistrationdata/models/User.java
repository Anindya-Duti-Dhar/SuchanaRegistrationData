package com.example.duti.suchanaregistrationdata.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User {
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @SerializedName("UserID")
    @Expose
    private String userId;

    public User(String userId){
        this.userId = userId;
    }
}
